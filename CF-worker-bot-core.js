/**
 * This is a part of Serverless Telegram Bot project by G.G.Pletnev
 * File CF-worker-bot-core.js - to be deployed as worker  
 * 
 */


addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})

const BOT_NAME = "myBotName"
/**
 * readRequestBody reads in the incoming request body
 * Use await readRequestBody(..) in an async function to get the string
 * @param {Request} request the incoming request to read from
 */
async function readRequestBody(request) {
  const { headers } = request
  const contentType = headers.get("content-type") || ""

  if (contentType.includes("application/json")) {
    return JSON.stringify(await request.json())
  }
  else {
    const myBlob = await request.blob()
    const objectURL = URL.createObjectURL(myBlob)
    return objectURL
  }
}
/**
 * Respond to the request
 * @param {Request} request
 */
async function handleRequest(request) {
     let reqBody = await readRequestBody(request);

// if any method accept POST - reply with 200 and self name
    if (request.method !== "POST") {
     return new Response(
          `Hi, I'm ${BOT_NAME}, a test bot` ,{status: 200}
       )
    }
// other case - it's a telegram call - respond with original content (echo)
    let body = JSON.parse(reqBody);
    let answer = {
           "method":"sendMessage",
           "chat_id": body.message.chat.id, 
           "reply_to_message_id" : body.message.message_id, 
           "text" :JSON.stringify(body.message)
    };

     return new Response(JSON.stringify(answer), 
      {headers: {
      "content-type": "application/json;charset=UTF-8",
    },status: 200}

    )

}